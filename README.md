# Cr4ckM3

Made by Thiago Molina with technologies like GCC, Vim, GTK+3, Make and Glade
Although the latter was not actually used in the project, it brought me some 
good ideas

### Description

Obviously, this is a joke. It is not protected, it is not obfuscated, it is not 
packaged, alias the source code is there. Anyone with a little notion of reverse 
engineering could break it. In fact, a chimp with a little will, and determination, 
could break it with brute force. So why did I do it?

For two reasons basically. The first is that I felt like playing with bitwise. 
Or as they say in my land "Escovar bits". The second, I explain below.
Importantly, this program uses an GTK interface. What is, in Linux is completely 
irrelevant, and is one more point to ensure that this is a joke... 

I was envious of the crackme's made for Windows...

That is, if you do not have GTK installed there, this will not work.

### Tip

Now comes the second reason. I would like to publicize the work of two people. 

Ladies First:

https://www.youtube.com/watch?v=vXWhB4TvWno

I am absurdly bad in math. I've always been. But with her, I learned some things. 
I can already understand 0 and 1. Now all I need to do is understand all the other 
numbers. Strange is that I do not understand a word of what she speaks, and look, 
theoretically we speak the same language. But I'm learning a lot from math, 
and that's good.

Now the second one:

https://www.youtube.com/watch?v=IkUfXfnnKH4

This guy is very good. And although I understand what he says, I can not understand 
what he does. Anyway, for those who want to start with reverse engineering, in Brazil, 
and need a material in Portuguese, this is the right place. Watch and support why 
this guy deserves. In fact, support the two, for they are really good at what they do.

That's it, they inspired me to play this game.

### Run it

clone, or download this repository, enter the directory and:

	$ make && ./Cr4ckM3

Good luck...

### Contact

For any clarification contact us

	Mail: t.molinex@gmail.com

