# Vars

PTH=$(PWD)/
HDR=$(path)hdr/
SRC=$(path)src/
DIR=@mkdir -p $(@D)
CC=gcc
PKGCONFIG=$(shell which pkg-config)
CFLAGS1=$(shell $(PKGCONFIG) --cflags gtk+-3.0)
LIBS=$(shell $(PKGCONFIG) --libs gtk+-3.0)
CFLAGS2=-W -Wall -ansi -std=c99
EXEC=Cr4ckM3

# build

all:$(EXEC)

$(PTH)bin/crack/*.o: $(HDR)crack.h
	$(DIR)
	@cd $(SRC)crack/ && $(CC) -c *.c $(CFLAGS2) && mv *.o $(PTH)bin/crack/

#$(PTH)bin/interface/*.o: $(HDR)interface.h $(HDR)crack.h
#	$(DIR)
#	@cd $(SRC)interface/ && $(CC) -c *.c $(CFLAGS1) && mv *.o $(PTH)bin/interface/

$(PTH)bin/main.o: $(SRC)main.c $(HDR)crack.h
	$(DIR)
	$(CC) -o $@ -c $< $(CFLAGS1)

$(EXEC): $(PTH)bin/main.o $(PTH)bin/crack/*.o
	$(CC) -o $@ $^ $(LIBS) 

# clean

clean:
	@rm -rf $(PTH)bin/
	@rm $(EXEC)
