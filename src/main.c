#include <gtk/gtk.h>
#include "../hdr/crack.h"

static GtkWidget *window = NULL;
static GtkWidget *info_c = NULL;
static GtkWidget *info_e = NULL;
static GtkWidget *entry = NULL;

static void
adios(GtkWidget *win)
{
	gtk_widget_destroy (win);
	gtk_main_quit();
}

static void
check_pass(GtkWidget *widget,
					 gpointer data)
{
	const char *pass = gtk_entry_get_text(GTK_ENTRY(entry));
	int tam = g_utf8_strlen(pass, -1);

	if (gtk_widget_get_visible(info_c))
		gtk_widget_hide (GTK_WIDGET (info_c));

	if (gtk_widget_get_visible(info_e))
		gtk_widget_hide (GTK_WIDGET (info_e));

	if (enterKey(pass, tam) == 1) {
		gtk_widget_show(GTK_WIDGET(info_c));
	} else {
		gtk_widget_show(GTK_WIDGET(info_e));
		gtk_entry_set_text(GTK_ENTRY(entry), "");
	}
}

GtkWidget *
do_intergface()
{
	GtkWidget *vbox;
	GtkWidget *label;
	GtkWidget *button;

	if (!window) {
		window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title(GTK_WINDOW(window), "Cr4ckM3");
		gtk_window_set_default_size (GTK_WINDOW (window), 300, 100);
		g_signal_connect(window, "destroy", G_CALLBACK(adios), NULL);
		gtk_container_set_border_width(GTK_CONTAINER(window), 8);

		vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
		gtk_container_add(GTK_CONTAINER(window), vbox);

		info_c = gtk_info_bar_new();
		gtk_box_pack_start(GTK_BOX(vbox), info_c, FALSE, FALSE, 0);
		gtk_info_bar_set_message_type(GTK_INFO_BAR(info_c), GTK_MESSAGE_INFO);

		label = gtk_label_new("Acerto, Miseravi!!!");
		gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
		gtk_label_set_xalign (GTK_LABEL (label), 0);
		gtk_box_pack_start (GTK_BOX (gtk_info_bar_get_content_area (GTK_INFO_BAR (info_c))), label, FALSE, FALSE, 0);

		info_e = gtk_info_bar_new();
		gtk_box_pack_start(GTK_BOX(vbox), info_e, FALSE, FALSE, 0);
		gtk_info_bar_set_message_type(GTK_INFO_BAR(info_e), GTK_MESSAGE_ERROR);

		label = gtk_label_new("Falhou na tarefa, mano!");
		gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
		gtk_label_set_xalign (GTK_LABEL (label), 0);
		gtk_box_pack_start (GTK_BOX (gtk_info_bar_get_content_area (GTK_INFO_BAR (info_e))), label, FALSE, FALSE, 0);

		entry = gtk_entry_new();
		gtk_box_pack_start(GTK_BOX(vbox), entry, FALSE, FALSE, 8);
		
		button = gtk_button_new_with_label ("Enviar"); 
		gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);
		g_signal_connect (button, "clicked", G_CALLBACK (check_pass), NULL);
	}

	if (!gtk_widget_get_visible (window)) {
		gtk_widget_show_all (window);
		gtk_widget_hide (GTK_WIDGET (info_c));
		gtk_widget_hide (GTK_WIDGET (info_e));
	}

	return window;
}

int 
main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);
	window = do_intergface();
	gtk_main();

	return 0;
}

