#include "../../hdr/crack.h"

int enterKey(const char key[], int tam)
{
	unsigned char init_pass = 0b111;
	int i, b;

	for (i = 0; i < tam; i++) {
		b = getBit(key[i]);

		if(verifyBit(init_pass, b))
			bitOff(&init_pass, b);
		else
			bitOn(&init_pass, b);
	}

	if ((tam % 2) != 0)
		reverseByte(&init_pass);

	return compareBytes(init_pass);
}

