#ifndef _CRACK_H
#define _CRACK_H

int getBit(char);
int verifyBit(unsigned char, unsigned int);
void bitOn(unsigned char *, unsigned int);
void bitOff(unsigned char *, unsigned int);
void reverseByte(unsigned char *);
int compareBytes(unsigned char);
int enterKey(const char [], int);

#endif

